const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    comments: { type: String },
    taskId: { type: Number, required: true },
    employeeId:  { 
        type    : mongoose.Schema.Types.ObjectId
    },
    status: { type: String, require: true}
    
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Task', schema);