const express = require('express');
const router = express.Router();
const userService = require('../services/user.service');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/task', submitTask);
router.post('/task/:id', updateTask);
router.post('/tasks', getEmployeeTasks);
router.get('/tasks/:id', getTasksById);


module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function submitTask(req, res, next) {
    userService.submitTask(req.body)
        .then(()=>res.json({}))
        .catch(err=>next(err));
}

function getTasksById(req, res, next) {
    userService.getTasksById(req.params.id)
        .then((tasks)=>res.json(tasks))
        .catch(err=>next(err));
}

function getEmployeeTasks(req, res, next) {
    userService.getEmployeeTasks(req.body)
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(err=>next(err));
}

function updateTask(req, res, next) {
    userService.updateTask(req.params.id, req.body)
        .then(()=>res.json({status: "OK"}))
        .catch(err=>next(err));
}