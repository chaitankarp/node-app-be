const config = require('../config/app.config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../db/mongoose');
const User = db.User;
const Task = db.Task;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    submitTask,
    getTasksById,
    getEmployeeTasks,
    updateTask,
    delete: _delete
};

async function authenticate({ username, password }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secretKey);
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(userParam) {
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    if (userParam.role === 'ROLE_EMPLOYEE' && !await User.findOne({username: userParam.SVusername})) {
        throw `Cannot find Supervisor with username: ${userParam.SVusername}`;
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }
    Object.assign(user, userParam);

    await user.save();
}

async function submitTask(userParams) {
    if (await Task.findOne({taskId: userParams.taskId})) {
        throw `Task with id '${userParams.taskId}' already submitted`;
    }

    const task = new Task(userParams);

    await task.save();
}

async function getTasksById(id) {
    return await Task.find({employeeId: id});
}

async function getEmployeeTasks(params) {    
    let tasks = [];
    const employeeIds = await User.find({SVusername: params.username}, {"_id": 1});
    if (employeeIds && employeeIds.length) {
        let ids = employeeIds.map(employee=>{
            return employee._id;
        });
        return await Task.find({employeeId: { "$in": ids }});
        
    }
    console.log(ids);
}

async function updateTask(id, userParam) {
    const task = await Task.findById(id);

    if (!task) {
        throw 'Task not found';
    }

    Object.assign(task, userParam);
    await task.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}
